def add_extras(feature_layer, version=''):
    extra_layers = []
    feature_transform_layers = []
    pyramid_feature_layers = []
    loc_layers = []
    conf_layers = []
    in_channels = None
    feature_transform_channel = int(feature_layer[0][1][-1] / 2)
    for layer, depth in zip(feature_layer[0][0], feature_layer[0][1]):
        print(layer, depth)
        if 'lite' in version:
            if layer == 'S':
                # extra_layers += [_conv_dw(in_channels, depth, stride=2, padding=1, expand_ratio=1)]
                in_channels = depth
            elif layer == '':
                # extra_layers += [_conv_dw(in_channels, depth, stride=1, expand_ratio=1)]
                in_channels = depth
            else:
                in_channels = depth
        else:
            if layer == 'S':
                # extra_layers += [
                #     nn.Conv2d(in_channels, int(depth / 2), kernel_size=1),
                #     nn.Conv2d(int(depth / 2), depth, kernel_size=3, stride=2, padding=1)]
                in_channels = depth
            elif layer == '':
                # extra_layers += [
                #     nn.Conv2d(in_channels, int(depth / 2), kernel_size=1),
                #     nn.Conv2d(int(depth / 2), depth, kernel_size=3)]
                in_channels = depth
            else:
                in_channels = depth
        print(in_channels, feature_transform_channel)
        # feature_transform_layers += [BasicConv(in_channels, feature_transform_channel, kernel_size=1, padding=0)]

    # in_channels = len(feature_transform_layers) * feature_transform_channel
    # for layer, depth, box in zip(feature_layer[1][0], feature_layer[1][1], mbox):
    #     if layer == 'S':
    #         pyramid_feature_layers += [BasicConv(in_channels, depth, kernel_size=3, stride=2, padding=1)]
    #         in_channels = depth
    #     elif layer == '':
    #         pad = (0, 1)[len(pyramid_feature_layers) == 0]
    #         pyramid_feature_layers += [BasicConv(in_channels, depth, kernel_size=3, stride=1, padding=pad)]
    #         in_channels = depth
    #     else:
    #         AssertionError('Undefined layer')
    #     loc_layers += [nn.Conv2d(in_channels, box * 4, kernel_size=3, padding=1)]
    #     conf_layers += [nn.Conv2d(in_channels, box * num_classes, kernel_size=3, padding=1)]
    # return base, extra_layers, (feature_transform_layers, pyramid_feature_layers), (loc_layers, conf_layers)


if __name__ == "__main__":
    feature_layers = [[[8, 12, 16], [256, 512, 1024]],
                    [['', 'S', 'S', 'S', '', ''], [512, 512, 256, 256, 256, 256]]]
    print(feature_layers[0])
    # add_extras(feature_layers[1])