from src.modeling import registry
from .box_head import SSDBoxHead, EfficientBoxHead

__all__ = ['build_box_head', 'SSDBoxHead', 'EfficientBoxHead']


def build_box_head(cfg):
    # TODO: make it more general
    return registry.BOX_HEADS[cfg.MODEL.BOX_HEAD](cfg)
