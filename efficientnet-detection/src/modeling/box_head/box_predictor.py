import torch
from torch import nn

from src.layers import SeparableConv2d
from src.layers.efficient_det.bifpn import BIFPN
from src.layers.efficient_det.header import Classifier, Regressor
from src.modeling import registry


class BoxPredictor(nn.Module):
    def __init__(self, cfg):
        super().__init__()
        self.cfg = cfg
        self.cls_headers = nn.ModuleList()
        self.reg_headers = nn.ModuleList()
        for level, (boxes_per_location, out_channels) in enumerate(
                zip(cfg.MODEL.PRIORS.BOXES_PER_LOCATION, cfg.MODEL.BACKBONE.OUT_CHANNELS)):
            self.cls_headers.append(self.cls_block(level, out_channels, boxes_per_location))
            self.reg_headers.append(self.reg_block(level, out_channels, boxes_per_location))
        self.reset_parameters()

    def cls_block(self, level, out_channels, boxes_per_location):
        raise NotImplementedError

    def reg_block(self, level, out_channels, boxes_per_location):
        raise NotImplementedError

    def reset_parameters(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.xavier_uniform_(m.weight)
                nn.init.zeros_(m.bias)

    def forward(self, features):
        cls_logits = []
        bbox_pred = []
        for feature, cls_header, reg_header in zip(features, self.cls_headers, self.reg_headers):
            cls_logits.append(cls_header(feature).permute(0, 2, 3, 1).contiguous())
            bbox_pred.append(reg_header(feature).permute(0, 2, 3, 1).contiguous())

        batch_size = features[0].shape[0]
        cls_logits = torch.cat([c.view(c.shape[0], -1) for c in cls_logits], dim=1).view(batch_size, -1,
                                                                                         self.cfg.MODEL.NUM_CLASSES)
        bbox_pred = torch.cat([l.view(l.shape[0], -1) for l in bbox_pred], dim=1).view(batch_size, -1, 4)

        return cls_logits, bbox_pred


@registry.BOX_PREDICTORS.register('SSDBoxPredictor')
class SSDBoxPredictor(BoxPredictor):
    def cls_block(self, level, out_channels, boxes_per_location):
        return nn.Conv2d(out_channels, boxes_per_location * self.cfg.MODEL.NUM_CLASSES, kernel_size=3, stride=1,
                         padding=1)

    def reg_block(self, level, out_channels, boxes_per_location):
        return nn.Conv2d(out_channels, boxes_per_location * 4, kernel_size=3, stride=1, padding=1)


@registry.BOX_PREDICTORS.register('SSDLiteBoxPredictor')
class SSDLiteBoxPredictor(BoxPredictor):
    def cls_block(self, level, out_channels, boxes_per_location):
        num_levels = len(self.cfg.MODEL.BACKBONE.OUT_CHANNELS)
        if level == num_levels - 1:
            return nn.Conv2d(out_channels, boxes_per_location * self.cfg.MODEL.NUM_CLASSES, kernel_size=1)
        return SeparableConv2d(out_channels, boxes_per_location * self.cfg.MODEL.NUM_CLASSES, kernel_size=3, stride=1,
                               padding=1)

    def reg_block(self, level, out_channels, boxes_per_location):
        num_levels = len(self.cfg.MODEL.BACKBONE.OUT_CHANNELS)
        if level == num_levels - 1:
            return nn.Conv2d(out_channels, boxes_per_location * 4, kernel_size=1)
        return SeparableConv2d(out_channels, boxes_per_location * 4, kernel_size=3, stride=1, padding=1)


@registry.BOX_PREDICTORS.register('EfficientBoxPredictor')
class EfficientBoxPredictor(nn.Module):
    def __init__(self, cfg):
        super().__init__()
        feature_layers = cfg.MODEL.FEATURE_LAYERS
        compound_coef = cfg.MODEL.COMPOUND_COEF
        num_channels = feature_layers[compound_coef]
        self.middle_headers = nn.Sequential(*[BIFPN(num_channels) for _ in range(min(2 + compound_coef, 8))])
        self.num_classes = cfg.MODEL.NUM_CLASSES
        self.cls_headers = Classifier(in_channels=num_channels, num_anchors=9,
                                      num_classes=self.num_classes,
                                      num_layers=3 + compound_coef // 3)
        self.reg_headers = Regressor(in_channels=num_channels, num_anchors=9,
                                     num_layers=3 + compound_coef // 3)

    def forward(self, features):
        features = self.middle_headers(features)

        bbox_pred = torch.cat([self.reg_headers(feature) for feature in features], dim=1)
        cls_logits = torch.cat([self.cls_headers(feature) for feature in features], dim=1)

        batch_size = features[0].shape[0]
        cls_logits = torch.cat([c.view(c.shape[0], -1) for c in cls_logits], dim=1).view(batch_size, -1,
                                                                                         self.num_classes)
        bbox_pred = torch.cat([l.view(l.shape[0], -1) for l in bbox_pred], dim=1).view(batch_size, -1, 4)

        return cls_logits, bbox_pred

# @registry.BOX_PREDICTORS.register('FSSDBoxPredictor')
# class FSSDBoxPredictor(SSDBoxPredictor):
#     def __init__(self, cfg):
#         super().__init__(cfg)
#         self.transforms = nn.ModuleList()
#         self.pyramids = nn.ModuleList()
#         self.norm = nn.BatchNorm2d(int(cfg.feature_layer[0][1][-1] / 2) * len(self.transforms), affine=True)
#         self.pre_build_module()
#
#     def pre_build_module(self):
#
# for out_feature in self.cfg.MODEL.BACKBONE.FEATURES: self.transforms.append(BasicConv(out_feature,
# self.cfg.feature_transform_channel, kernel_size=1, padding=0))
#
#         in_channels = len(self.transforms) * self.cfg.feature_transform_channel
#         self.pyramids.append(BasicConv(in_channels))
#
#     def forward(self, features):
#         transformed = []
#         pyramids = []
#         assert len(self.transforms) == len(features)
#         upsize = (features[0].size()[2], features[0].size()[3])
#
#         for k, layer in enumerate(self.transforms):
#             size = None if k == 0 else upsize
#             transformed.append(layer(features[k], size))
#
#         outputs = torch.cat(transformed, 1)
#         outputs = self.norm(outputs)
#
#         for k, layer in enumerate(self.pyramids):
#             outputs = layer(outputs)
#             pyramids.append(outputs)
#
#         return super(FSSDBoxPredictor, self).forward(pyramids)