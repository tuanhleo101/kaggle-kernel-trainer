from src.modeling import registry
from src.modeling.backbone.mobilenet import MobileNetV2, MobileNetV3
from .efficient_net import efficient_net_b0, efficient_lite_b0, efficient_lite_b1, efficient_lite_b2, efficient_lite_b3, \
    efficient_lite_b4

__all__ = ['build_backbone', 'MobileNetV2', 'MobileNetV3', 'efficient_net_b0', 'efficient_lite_b0', 'efficient_lite_b1',
           'efficient_lite_b2', 'efficient_lite_b3', 'efficient_lite_b4']


def build_backbone(cfg):
    return registry.BACKBONES[cfg.MODEL.BACKBONE.NAME](cfg, cfg.MODEL.BACKBONE.PRETRAINED)
