from abc import ABC

import torch
import torch.nn.functional as F

from src.losses.my_multibox_loss import MultiBoxLoss
from src.losses.utils import RepulsionLoss, prediction_match


class RepulsionMultiBoxLoss(MultiBoxLoss, ABC):
    def __init__(self, num_classes, neg_pos_ratio=3, variance=(0.1, 0.2), use_gpu=True, *args, **kwargs):
        super().__init__(num_classes, neg_pos_ratio, use_gpu, *args, **kwargs)
        self.variance = variance

    def get_conf_fn(self):
        return self.get_confidence_loss

    def get_loc_fn(self):
        return self.get_repGT_loss

    def prior_match(self, predictions, loc_targets, priors=None):
        loc_g = torch.empty(predictions.size())
        batch_size = predictions.size()[0]
        for idx in range(batch_size):
            prediction = predictions[idx]
            truth = loc_targets[idx]
            loc_g[idx] = prediction_match(prediction, truth, priors, self.variance)
        return predictions

    def get_repGT_loss(self, predicted_locations, loc_targets, labels, priors):
        pos_mask = labels > 0  # ignore background
        pos_idx = pos_mask.unsqueeze(pos_mask.dim()).expand_as(predicted_locations)
        matched_predicted_locations = self.prior_match(predicted_locations, loc_targets, priors)
        loc_p = predicted_locations[pos_idx].view(-1, 4)
        loc_t = loc_targets[pos_idx].view(-1, 4)
        loc_g = matched_predicted_locations[pos_mask].view(-1, 4)

        loc_loss = F.smooth_l1_loss(loc_p, loc_t, reduction='sum')
        repGT_loss = RepulsionLoss(sigma=0.)(loc_p, loc_g, priors)
        loss_dict = {"reg_loss": loc_loss, "repGT_loss": repGT_loss}
        return loss_dict

    def get_repBox_loss(self, predicted_locations, loc_targets, labels, priors):
        # TODO: implement repulsion loss for predicted boxes
        return None
