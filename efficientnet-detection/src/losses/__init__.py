from src.losses.multibox_loss import MultiBoxLoss
from src.losses.focal_loss import FocalLoss
from src.losses.multibox_repulsion_loss import RepulsionMultiBoxLoss
from src.losses.focal_repulsion_loss import RepulsionFocalLoss

__all__ = ["get_loss", "MultiBoxLoss", "FocalLoss", "RepulsionFocalLoss", "RepulsionMultiBoxLoss"]


def get_loss(cfg):
    print('edexdex', cfg.SOLVER.LOSS_PARAMS)
    return globals().get(cfg.SOLVER.LOSS)(cfg.MODEL.NUM_CLASSES, **cfg.SOLVER.LOSS_PARAMS)
