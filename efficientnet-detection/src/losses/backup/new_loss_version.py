# from abc import ABC
# from functools import partial
#
# import torch
# import torch.nn as nn
# import torch.nn.functional as F
#
# from .utils import hard_negative_mining, log_sum_exp, RepulsionLoss
#
#
# class AbstractionLoss(nn.Module):
#     def __init__(self, num_classes, use_gpu, *args, **kwargs):
#         super().__init__()
#         self.num_classes = num_classes
#         self.use_gpu = use_gpu
#
#     def get_conf_fn(self):
#         raise NotImplementedError
#
#     def get_loc_fn(self):
#         raise NotImplementedError
#
#     def forward(self, pred_conf, pred_loc, target_label, target_loc, **others):
#         """
#         :param pred_conf: [batch, n_anchors, num_classes]
#         :param pred_loc: [batch, n_anchors, 4]
#         :param target_label: [batch, n_anchors]
#         :param target_loc: [batch, n_anchors, 4]
#         :return:
#         """
#         assert pred_conf.size(2) == self.num_classes
#         ignores = target_label < 0
#         target_label[ignores] = 0
#
#         conf_loss = self.get_conf_fn()(pred_conf, target_label)
#         loc_loss = partial(self.get_conf_fn(), pred_loc, target_loc, target_label)(**others)
#
#         pos_mask = target_label > 0
#         num_pos = pos_mask.long().sum(1, keepdim=True)
#         # It is caused by data augmentation when crop the images. The cropping can distort the boxes
#         N = max(num_pos.data.sum(), 1)  # to avoid divide by 0.
#         # TODO: tuple / int
#         return loc_loss / N, conf_loss / N
#
#
# class MultiBoxLoss(AbstractionLoss, ABC):
#     def __init__(self, num_classes, neg_pos_ratio, use_gpu=True, *args, **kwargs):
#         super().__init__(num_classes, use_gpu, *args, **kwargs)
#         self.neg_pos_ratio = neg_pos_ratio
#
#     def get_conf_fn(self):
#         return self.get_confidence_loss
#
#     def get_loc_fn(self):
#         return self.get_regression_loss
#
#     def get_confidence_loss(self, pred_conf, target_label):
#         batch_conf = pred_conf.view(-1, pred_conf.size(2))
#         gathered_conf = batch_conf.gather(1, target_label.view(-1, 1))
#         loss_c = log_sum_exp(batch_conf) - gathered_conf
#
#         with torch.no_grad():
#             pos_mask, neg_mask = hard_negative_mining(loss_c, target_label, self.neg_pos_ratio)
#
#         pos_idx = pos_mask.unsqueeze(2).expand_as(pred_conf)
#         neg_idx = neg_mask.unsqueeze(2).expand_as(pred_conf)
#         conf_p = pred_conf[(pos_idx + neg_idx).gt(0)].view(-1, pred_conf.size(2))
#         targets_weighted = target_label[(pos_mask + neg_mask).gt(0)]
#         conf_loss = F.cross_entropy(conf_p, targets_weighted, reduction='sum')
#         return conf_loss
#
#     @staticmethod
#     def get_regression_loss(pred_loc, target_loc, target_label):
#         pos_mask = target_label > 0  # ignore background
#         pos_idx = pos_mask.unsqueeze(pos_mask.dim()).expand_as(pred_loc)
#         loc_p = pred_loc[pos_idx].view(-1, 4)
#         loc_t = target_loc[pos_idx].view(-1, 4)
#         loc_loss = F.smooth_l1_loss(loc_p, loc_t, reduction='sum')
#         return loc_loss
#
#     def forward(self, pred_conf, pred_loc, target_label, target_loc, **others):
#         assert pred_conf.size(2) == self.num_classes
#         ignores = target_label < 0
#         target_label[ignores] = 0
#
#         # Confidence loss including pos and neg examples
#         conf_loss = self.get_confidence_loss(pred_conf, target_label)
#
#         # Localization Loss part
#         pos_mask = target_label > 0
#         loc_loss = self.get_regression_loss(pred_loc, target_loc, target_label)
#         num_pos = pos_mask.long().sum(1, keepdim=True)
#         # It is caused by data augmentation when crop the images. The cropping can distort the boxes
#         N = max(num_pos.data.sum(), 1)  # to avoid divide by 0.
#         return loc_loss / N, conf_loss / N
#
#
# class FocalLoss(AbstractionLoss, ABC):
#     def __init__(self, num_classes, gamma=2, alpha=0.25, use_gpu=True, *args, **kwargs):
#         super().__init__(num_classes, use_gpu, *args, **kwargs)
#         self.gamma = gamma
#         self.alpha = alpha
#
#     def get_conf_fn(self):
#         return self.get_focal_loss
#
#     def get_loc_fn(self):
#         return self.get_regression_loss
#
#     def get_focal_loss(self, pred_conf, target_label):
#         assert pred_conf.size(2) == self.num_classes
#         ignores = target_label < 0
#         target_label[ignores] = 0
#
#         pos_cls = target_label > -1
#         mask = pos_cls.unsqueeze(2).expand_as(pred_conf)
#         conf_p = pred_conf[mask].view(-1, pred_conf.size(2)).clone()
#         p_t_log = -F.cross_entropy(conf_p, target_label[pos_cls], reduction='sum')
#         p_t = torch.exp(p_t_log)
#
#         # This is focal loss presented in the paper eq(5)
#         conf_loss = -self.alpha * ((1 - p_t) ** self.gamma * p_t_log)
#         return conf_loss
#
#     def forward(self, pred_conf, pred_loc, target_label, target_loc, **others):
#         # get focal loss
#         conf_loss = self.get_focal_loss(pred_conf, target_label)
#
#         # Localization Loss part
#         pos_mask = target_label > 0
#         loc_loss = self.get_regression_loss(pred_loc, target_loc, target_label)
#
#         num_pos = pos_mask.long().sum(1, keepdim=True)
#         # It is caused by data augmentation when crop the images. The cropping can distort the boxes
#         N = max(num_pos.data.sum(), 1)  # to avoid divide by 0.
#         return loc_loss / N, conf_loss / N
#
#
# class RepulsionMultiBoxLoss(MultiBoxLoss, ABC):
#     def __init__(self, num_classes, use_gpu=True, *args, **kwargs):
#         super().__init__(num_classes, use_gpu, *args, **kwargs)
#
#     def get_conf_fn(self):
#         return self.get_confidence_loss
#
#     def get_loc_fn(self):
#         return self.get_repGT_loss
#
#     @staticmethod
#     def prior_match(predictions, priors=None):
#         return predictions
#
#     @staticmethod
#     def get_repGT_loss(pred_loc, target_loc, pos_mask, priors):
#         matched_predicted_locations = RepulsionMultiBoxLoss.prior_match(pred_loc, priors)
#         pos_idx = pos_mask.unsqueeze(pos_mask.dim()).expand_as(pred_loc)
#         loc_p = pred_loc[pos_idx].view(-1, 4)
#         loc_t = target_loc[pos_idx].view(-1, 4)
#         loc_g = matched_predicted_locations[pos_mask].view(-1, 4)
#
#         loc_loss = F.smooth_l1_loss(loc_p, loc_t, reduction='sum')
#         repGT_loss = RepulsionLoss(sigma=0.)(loc_p, loc_g, priors)
#         return loc_loss, repGT_loss
#
#     @staticmethod
#     def get_repBox_loss(pred_loc, target_loc, pos_mask, priors):
#         # TODO
#         return None
#
#     def forward(self, pred_conf, pred_loc, target_label, target_loc, **others):
#         priors = others.get("priors", None)
#         assert priors is None
#
#         assert pred_conf.size(2) == self.num_classes
#         ignores = target_label < 0
#         target_label[ignores] = 0
#
#         # Confidence loss including pos and neg examples
#         conf_loss = self.get_confidence_loss(pred_conf, target_label)
#
#         # Localization Loss part(get repgt loss)
#         pos_mask = target_label > 0  # ignore background
#         loc_loss, repGT_loss = self.get_repGT_loss(pred_loc, target_loc, pos_mask, priors)
#
#         # Sum of losses: L(x,c,l,g) = (conf_loss(x, c) + αloc_loss(x,l,g)) / N
#         num_pos = pos_mask.long().sum(1, keepdim=True)
#         # It is caused by data augmentation when crop the images. The cropping can distort the boxes
#         N = max(num_pos.data.sum(), 1)  # to avoid divide by 0.
#         return loc_loss / N, conf_loss / N, repGT_loss / N
#
#
# class RepulsionFocalLoss(FocalLoss, FocalLoss):
#     def __init__(self, num_classes, use_gpu=True, *args, **kwargs):
#         super().__init__(num_classes, use_gpu, *args, **kwargs)
#
#     def get_conf_fn(self):
#         return self.get_focal_loss
#
#     def get_loc_fn(self):
#         return self.get_repGT_loss
#
#     def forward(self, pred_conf, pred_loc, target_label, target_loc, **others):
#         priors = others.get("priors", None)
#         assert priors is None
#
#         assert pred_conf.size(2) == self.num_classes
#         ignores = target_label < 0
#         target_label[ignores] = 0
#
#         # get focal loss
#         conf_loss = self.get_focal_loss(pred_conf, target_label)
#
#         # Localization Loss part(get repgt loss)
#         pos_mask = target_label > 0  # ignore background
#         loc_loss, repGT_loss = partial(self.get_repGT_loss, pred_loc, target_loc, pos_mask)(priors)
#
#         # Sum of losses: L(x,c,l,g) = (conf_loss(x, c) + αloc_loss(x,l,g)) / N
#         num_pos = pos_mask.long().sum(1, keepdim=True)
#         # It is caused by data augmentation when crop the images. The cropping can distort the boxes
#         N = max(num_pos.data.sum(), 1)  # to avoid divide by 0.
#         return loc_loss / N, conf_loss / N, repGT_loss / N
