import random

import numpy as np
import torch

from .focal_loss import FocalLoss
from .utils import IoG, smooth_ln, calc_iou


class RepulsionFocalLoss(FocalLoss):
    def __init__(self, anchor, num_classes=2, use_gpu=False, alpha=0.25, gamma=2.0):
        super().__init__(anchor, num_classes, use_gpu, alpha, gamma)

    def get_repulsion_loss(self, regression, bbox_annotation, positive_indices, IoU, IoU_argmax):
        RepBox_loss = torch.tensor(0).float()
        RepGT_loss = torch.tensor(0).float()
        if positive_indices.sum() > 0:
            if bbox_annotation.shape[0] == 1:
                if self.use_gpu:
                    RepBox_loss = RepBox_loss.cuda()
                    RepGT_loss = RepGT_loss.cuda()
            else:
                anchor_widths_pi = self.anchor_widths[positive_indices]
                anchor_heights_pi = self.anchor_heights[positive_indices]
                anchor_ctr_x_pi = self.anchor_ctr_x[positive_indices]
                anchor_ctr_y_pi = self.anchor_ctr_y[positive_indices]

                regression_pos = regression[positive_indices, :]
                regression_pos_dx = regression_pos[:, 0]
                regression_pos_dy = regression_pos[:, 1]
                regression_pos_dw = regression_pos[:, 2]
                regression_pos_dh = regression_pos[:, 3]
                predict_w = torch.exp(regression_pos_dw) * anchor_widths_pi
                predict_h = torch.exp(regression_pos_dh) * anchor_heights_pi
                predict_x = regression_pos_dx * anchor_widths_pi + anchor_ctr_x_pi
                predict_y = regression_pos_dy * anchor_heights_pi + anchor_ctr_y_pi
                predict_xmin = predict_x - 0.5 * predict_w
                predict_ymin = predict_y - 0.5 * predict_h
                predict_xmax = predict_x + 0.5 * predict_w
                predict_ymax = predict_y + 0.5 * predict_h
                predict_boxes = torch.stack((predict_xmin, predict_ymin, predict_xmax, predict_ymax)).t()

                # add RepGT_losses
                IoU_pos = IoU[positive_indices, :]
                IoU_max_keep, IoU_argmax_keep = torch.max(IoU_pos, dim=1, keepdim=True)  # num_anchors x 1
                for idx in range(IoU_argmax_keep.shape[0]):
                    IoU_pos[idx, IoU_argmax_keep[idx]] = -1
                IoU_sec, IoU_argsec = torch.max(IoU_pos, dim=1)

                assigned_annotations_sec = bbox_annotation[IoU_argsec,
                                           :]  # which gt the anchor iou second num_anchors * 5

                IoG_to_minimize = IoG(assigned_annotations_sec, predict_boxes)
                RepGT_loss = smooth_ln(IoG_to_minimize, 0.5)
                RepGT_loss = RepGT_loss.mean()

                # add PepBox losses
                IoU_argmax_pos = IoU_argmax[positive_indices].float()
                IoU_argmax_pos = IoU_argmax_pos.unsqueeze(0).t()
                predict_boxes = torch.cat([predict_boxes, IoU_argmax_pos], dim=1)
                predict_boxes_np = predict_boxes.detach().cpu().numpy()
                num_gt = bbox_annotation.shape[0]
                predict_boxes_sampled = []
                for id in range(num_gt):
                    index = np.where(predict_boxes_np[:, 4] == id)[0]
                    if index.shape[0]:
                        idx = random.choice(range(index.shape[0]))
                        predict_boxes_sampled.append(predict_boxes[index[idx], :4])
                predict_boxes_sampled = torch.stack(predict_boxes_sampled)
                iou_repbox = calc_iou(predict_boxes_sampled, predict_boxes_sampled)
                mask = torch.lt(iou_repbox, 1.).float()
                iou_repbox = iou_repbox * mask
                RepBox_loss = smooth_ln(iou_repbox, 0.5)
                RepBox_loss = RepBox_loss.sum() / torch.clamp(torch.sum(torch.gt(iou_repbox, 0)).float(), min=1.0)

        return RepGT_loss, RepBox_loss

    def forward(self, predicted_locations, confidences, annotations, ignores=None, **kwargs):

        batch_size = predicted_locations.size()[0]
        self.prepare()

        conf_losses = []
        loc_losses = []
        RepGT_losses = []
        RepBox_losses = []

        for j in range(batch_size):
            confidence = confidences[j, :, :]
            regression = predicted_locations[j, :, :]

            bbox_annotation = annotations[j, :, :]
            bbox_annotation = bbox_annotation[bbox_annotation[:, 4] != -1]

            if bbox_annotation.shape[0] == 0:
                loc_losses.append(torch.tensor(0).float().cuda())
                conf_losses.append(torch.tensor(0).float().cuda())
                RepGT_losses.append(torch.tensor(0).float().cuda())
                RepBox_losses.append(torch.tensor(0).float().cuda())
                continue

            confidence = torch.clamp(confidence, 1e-4, 1.0 - 1e-4)
            IoU = calc_iou(self.anchor, bbox_annotation[:, :4])  # num_anchors x num_annotations
            IoU_max, IoU_argmax = torch.max(IoU, dim=1)  # num_anchors x 1

            assert IoU_max.size(0) == self.anchor.size(0)

            if ignores:
                confidence, regression = self.filter(confidence, regression, ignores[j])

            cls_loss, positive_indices, assigned_annotations = self.get_focal_loss(confidence, bbox_annotation,
                                                                                   IoU_max, IoU_argmax)

            conf_losses.append(cls_loss)
            loc_losses.append(self.compute_regression_loss(regression, assigned_annotations, positive_indices))

            RepGT_loss, RepBox_loss = self.get_repulsion_loss(regression, bbox_annotation, positive_indices, IoU,
                                                              IoU_argmax)
            RepGT_losses.append(RepGT_loss)
            RepBox_losses.append(RepBox_loss)

        return torch.stack(conf_losses).mean(dim=0, keepdim=True), \
               torch.stack(loc_losses).mean(dim=0, keepdim=True), \
               torch.stack(RepGT_losses).mean(dim=0, keepdim=True), \
               torch.stack(RepBox_losses).mean(dim=0, keepdim=True)
