import torch

from .loss_base import LossAbstraction
from .utils import calc_iou


class FocalLoss(LossAbstraction):
    def __init__(self, anchor, num_classes=2, use_gpu=False, alpha=0.25, gamma=2.0):
        super().__init__(anchor, num_classes, use_gpu, alpha, gamma)

    def get_focal_loss(self, classification, bbox_annotation, IoU_max, IoU_argmax):
        targets = torch.ones(classification.shape) * -1
        if self.use_gpu:
            targets = targets.cuda()

        targets[torch.lt(IoU_max, 0.4)] = 0
        positive_indices = torch.ge(IoU_max, 0.5)

        assigned_annotations = bbox_annotation[IoU_argmax, :]

        targets[positive_indices, :] = 0
        targets[positive_indices, assigned_annotations[positive_indices, 4].long] = 1

        if self.use_gpu:
            alpha_factor = torch.ones(targets.shape).cuda() * self.alpha
        else:
            alpha_factor = torch.ones(targets.shape) * self.alpha

        alpha_factor = torch.where(torch.eq(targets, 1.), alpha_factor, 1. - alpha_factor)
        focal_weight = torch.where(torch.eq(targets, 1.), 1.0 - classification, classification)
        focal_weight = alpha_factor * torch.pow(focal_weight, self.gamma)

        bce = -(targets * torch.log(classification) + (1.0 - targets) * torch.log(1.0 - classification))

        cls_loss = focal_weight * bce

        if self.use_gpu:
            cls_loss = torch.where(torch.ne(targets, -1.0), cls_loss, torch.zeros(cls_loss.shape).cuda())
        else:
            cls_loss = torch.where(torch.ne(targets, -1.0), cls_loss, torch.zeros(cls_loss.shape))

        return cls_loss.sum() / torch.clamp(positive_indices.sum().float(),
                                            min=1.0), positive_indices, assigned_annotations

    def forward(self, predictions, annotations, ignores=None):
        predicted_locations, confidences = predictions
        batch_size = predicted_locations.size()[0]

        conf_losses = []
        loc_losses = []

        for j in range(batch_size):
            confidence = confidences[j, :, :]
            regression = predicted_locations[j, :, :]

            bbox_annotation = annotations[j, :, :]
            bbox_annotation = bbox_annotation[bbox_annotation[:, 4] != -1]

            if bbox_annotation.shape[0] == 0:
                loc_losses.append(torch.tensor(0).float().cuda())
                conf_losses.append(torch.tensor(0).float().cuda())
                continue

            confidence = torch.clamp(confidence, 1e-4, 1.0 - 1e-4)
            IoU = calc_iou(self.anchor, bbox_annotation[:, :4])  # num_anchors x num_annotations
            IoU_max, IoU_argmax = torch.max(IoU, dim=1)  # num_anchors x 1

            assert IoU_max.size(0) == self.anchor.size(0)

            cls_loss, positive_indices, assigned_annotations = self.get_focal_loss(confidence, bbox_annotation,
                                                                                   IoU_max, IoU_argmax)

            conf_losses.append(cls_loss)
            loc_losses.append(self.compute_regression_loss(regression, assigned_annotations, positive_indices))

        return torch.stack(conf_losses).mean(dim=0, keepdim=True), torch.stack(loc_losses).mean(dim=0, keepdim=True)
