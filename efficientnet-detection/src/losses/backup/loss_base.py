import torch
from torch import nn

from .utils import calc_iou


class LossAbstraction(nn.Module):
    def __init__(self, anchor, num_classes, use_gpu, alpha, gamma):
        super().__init__()
        self.anchor = anchor
        self.num_classes = num_classes
        self.use_gpu = use_gpu
        self.alpha = alpha
        self.gamma = gamma

    def prepare(self):
        self.anchor_widths = self.anchor[:, 2] - self.anchor[:, 0]
        self.anchor_heights = self.anchor[:, 3] - self.anchor[:, 1]
        self.anchor_ctr_x = self.anchor[:, 0] + 0.5 * self.anchor_widths
        self.anchor_ctr_y = self.anchor[:, 1] + 0.5 * self.anchor_heights

    def filter(self, classification, regression, ignore=None):
        ignore = ignore[ignore[:, 4] != -1]
        if ignore.shape[0] > 0:
            iou_igno = calc_iou(self.anchor, ignore)
            iou_igno_max, iou_igno_argmax = torch.max(iou_igno, dim=1)
            index_igno = torch.lt(iou_igno_max, 0.5)
            self.anchor = self.anchor[index_igno, :]
            classification = classification[index_igno, :]
            regression = regression[index_igno, :]
            self.anchor_widths = self.anchor_widths[index_igno]
            self.anchor_heights = self.anchor_heights[index_igno]
            self.anchor_ctr_x = self.anchor_ctr_x[index_igno]
            self.anchor_ctr_y = self.anchor_ctr_y[index_igno]

        return classification, regression

    def compute_regression_loss(self, regression, assigned_annotations, positive_indices):
        if positive_indices.sum() > 0:
            assigned_annotations = assigned_annotations[positive_indices, :]

            anchor_widths_pi = self.anchor_widths[positive_indices]
            anchor_heights_pi = self.anchor_heights[positive_indices]
            anchor_ctr_x_pi = self.anchor_ctr_x[positive_indices]
            anchor_ctr_y_pi = self.anchor_ctr_y[positive_indices]

            gt_widths = assigned_annotations[:, 2] - assigned_annotations[:, 0]
            gt_heights = assigned_annotations[:, 3] - assigned_annotations[:, 1]
            gt_ctr_x = assigned_annotations[:, 0] + 0.5 * gt_widths
            gt_ctr_y = assigned_annotations[:, 1] + 0.5 * gt_heights

            # clip widths to 1
            gt_widths = torch.clamp(gt_widths, min=1)
            gt_heights = torch.clamp(gt_heights, min=1)

            targets_dx = (gt_ctr_x - anchor_ctr_x_pi) / anchor_widths_pi
            targets_dy = (gt_ctr_y - anchor_ctr_y_pi) / anchor_heights_pi
            targets_dw = torch.log(gt_widths / anchor_widths_pi)
            targets_dh = torch.log(gt_heights / anchor_heights_pi)

            targets = torch.stack(
                (targets_dx, targets_dy, targets_dw, targets_dh))
            targets = targets.t()

            if self.use_gpu:
                targets = targets / torch.Tensor([[0.1, 0.1, 0.2, 0.2]]).cuda()
            else:
                targets = targets / torch.Tensor([[0.1, 0.1, 0.2, 0.2]])

            # negative_indices = 1 + (~positive_indices)

            regression_diff = torch.abs(
                targets - regression[positive_indices, :])

            regression_loss = torch.where(
                torch.le(regression_diff, 1.0 / 9.0),
                0.5 * 9.0 * torch.pow(regression_diff, 2),
                regression_diff - 0.5 / 9.0
            )
            return regression_loss.mean()
        else:
            if self.use_gpu:
                return torch.tensor(0).float().cuda()
            else:
                torch.tensor(0).float()

    def forward(self, predictions, annotations, **kwargs):
        raise NotImplementedError
