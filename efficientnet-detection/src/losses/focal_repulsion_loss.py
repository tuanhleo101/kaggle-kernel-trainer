from abc import ABC

from src.losses import FocalLoss, RepulsionMultiBoxLoss
from src.losses.my_multibox_loss import MultiBoxLoss


class RepulsionFocalLoss(MultiBoxLoss, ABC):
    def __init__(self, num_classes, variance=(0.1, 0.2), neg_pos_ratio=3, gamma=2, alpha=0.25,
                 use_gpu=True, *args, **kwargs):
        super().__init__(num_classes, neg_pos_ratio, use_gpu, *args, **kwargs)
        self.focal_loss = FocalLoss(num_classes, neg_pos_ratio, gamma, alpha, use_gpu, *args, **kwargs)
        self.repulsion_loss = RepulsionMultiBoxLoss(num_classes, neg_pos_ratio, variance, use_gpu, *args, **kwargs)

    def get_conf_fn(self):
        return self.focal_loss.get_focal_loss

    def get_loc_fn(self):
        return self.repulsion_loss.get_repGT_loss


if __name__ == "__main__":
    loss_fn = RepulsionFocalLoss(num_classes=2)
