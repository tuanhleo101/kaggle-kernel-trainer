import glob
import os
import unittest

import cv2
import numpy as np
from tqdm import tqdm

from src.config.defaults import _C as cfg
from src.data.build import make_data_loader
from src.data.datasets import SmartShop
from src.data.datasets.mot import MOTDet
from src.data.transforms import build_transforms, build_target_transform
from src.utils.data_utils import get_boxes_from_file, get_boxes_from_annotation


def drawBox(boxes, image, is_plot=True, new_file=""):
    for i in range(0, len(boxes)):
        # changed color and width to make it visible
        cv2.rectangle(image, (boxes[i][0], boxes[i][1]), (boxes[i][2], boxes[i][3]), (255, 0, 0), 1)
    if is_plot:
        cv2.imshow("img1", image)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
    else:
        cv2.imwrite(new_file, image)


class DataTester(unittest.TestCase):
    data_root = "/mnt/D466DA6A66DA4CBC/2020/schools/efficientnet-detection/datasets"
    mot_dir = "mot"
    train_transform = build_transforms(cfg, is_train=True)
    target_transform = build_target_transform(cfg)
    frame_sz = (1280, 720)

    def testMOTDataset(self):
        mot_data_root = os.path.join(self.data_root, "mot", "train")
        seq_mot = "processed"
        data = MOTDet(mot_data_root, seq_mot, self.train_transform, self.target_transform)
        for index in tqdm(range(len(data))):
            print(data[index][1]["boxes"].size())

    def testShopDataset0(self):
        data_root = "/mnt/D466DA6A66DA4CBC/2020/schools/efficientnet-detection/datasets/smart_shop/"
        data = SmartShop(data_dir=data_root, split="val")
        for index in tqdm(range(len(data))):
            if data[index][1]["boxes"].shape[0] == 0:
                print(data[index][3])
            # print(data[index][1]["boxes"].size())

    def testShopDataset1(self):
        data_root = "/mnt/D466DA6A66DA4CBC/2020/schools/efficientnet-detection/datasets/smart_shop/"
        tmp = "/mnt/D466DA6A66DA4CBC/2020/schools/efficientnet-detection/datasets/check_data/"
        data = SmartShop(data_dir=data_root, split="train")
        for index in tqdm(range(len(data))):
            drawBox(data[index][1]["boxes"], data[index][0], False, f"{tmp}/{index}.jpg")

    def testDataLoader(self):
        mot_data_root = os.path.join(self.data_root, "mot", "train")
        cfg.merge_from_file(
            "/mnt/D466DA6A66DA4CBC/2020/schools/efficientnet-detection/configs/test.yaml")
        data_loader = make_data_loader(cfg, True, max_iter=1000)
        for data in data_loader:
            print(data)
            break

    def testShowSImage(self):
        image_file = "/mnt/D466DA6A66DA4CBC/2020/schools/efficientnet-detection/datasets/mot/train/MOT20-01/img1" \
                     "/000001.jpg"
        img = cv2.imread(image_file)
        boxes = get_boxes_from_file(
            "/mnt/D466DA6A66DA4CBC/2020/schools/efficientnet-detection/datasets/mot/train/MOT20-01/gt/gt.txt",
            img.shape)[0][image_file.split("/")[-1].replace(".jpg", "")]

        for box in boxes:
            cv2.rectangle(img, (int(box[0]), int(box[1])), (int(box[2]), int(box[3])), color=(255, 0, 0), thickness=1)
            cv2.putText(img, f'{box[5]}', (int(box[0]), int(box[1])), cv2.FONT_ITALIC, 0.7, (0, 255, 0), 1)
        img = cv2.resize(img, DataTester.frame_sz)
        cv2.imshow("sample_show", img)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

    def testResizeImage(self):
        image_file = "/mnt/D466DA6A66DA4CBC/2020/schools/efficientnet-detection/datasets/mot/train/MOT20-01/img1" \
                     "/000001.jpg"
        targetSize = (720, 920)
        original = cv2.imread(image_file, 3)
        box = get_boxes_from_file(
            "/mnt/D466DA6A66DA4CBC/2020/schools/efficientnet-detection/datasets/mot/train/MOT20-01/gt/gt.txt",
            original.shape)[0][
            image_file.split("/")[-1].replace(".jpg", "")][8]
        box = [int(x) for x in box[:4]]

        y_ = original.shape[0]
        x_ = original.shape[1]

        x_scale = targetSize[0] / x_
        y_scale = targetSize[1] / y_
        img = cv2.resize(original, targetSize)
        img = np.array(img)
        # box[0] = int(np.round(box[0] * x_scale))
        # box[1] = int(np.round(box[1] * y_scale))
        # box[2] = int(np.round(box[2] * x_scale))
        # box[3] = int(np.round(box[3] * y_scale))
        drawBox([[1, 0, box[0], box[1], box[2], box[3]]], img)

    def testShowSample(self):
        temp_dir = "/mnt/D466DA6A66DA4CBC/2020/schools/efficientnet-detection/datasets/tmp/KITTI-17_ver0"
        img_files = os.listdir(os.path.join(temp_dir, "img1"))
        for file in img_files:
            img = cv2.imread(os.path.join(temp_dir, "img1", file), 3)
            boxes = get_boxes_from_annotation(os.path.join(temp_dir, "ann", file.replace("jpg", "txt")))
            for box in boxes:
                cv2.rectangle(img, (box[0], box[1]), (box[2], box[3]), (255, 0, 0), 2)
            cv2.imshow("", img)
            cv2.waitKey(0)
            cv2.destroyAllWindows()

    def testCheckBoxSize(self):
        ann_dir = "/mnt/D466DA6A66DA4CBC/2020/schools/efficientnet-detection/datasets/mot/train/processed/ann/"
        files = glob.glob(os.path.join(ann_dir, "*.txt"))
        for file in files:
            is_empty = True
            with open(file, "r") as f:
                for line in f.readlines():
                    is_empty = False
            if is_empty:
                print(file)
