import unittest

import torch

from src.config.defaults import _C as cfg
from src.modeling.detector import build_detection_model


class TestDetector(unittest.TestCase):
    def testEfficientNet(self):
        cfg.merge_from_file(
            "/mnt/D466DA6A66DA4CBC/2020/schools/efficientnet-detection/configs/efficient_lite_b2_ssd300_mot0712.yaml")
        model = build_detection_model(cfg)
        inputs = torch.rand(2, 3, 300, 300)
        outputs = model(inputs)
