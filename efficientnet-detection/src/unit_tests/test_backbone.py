import unittest

import torch

from src.config.defaults import _C as cfg
from src.modeling.backbone import MobileNetV2, MobileNetV3
from src.modeling.backbone.efficient_net import efficient_lite_b0, efficient_net_b0, efficient_lite_b1, \
    efficient_lite_b2


class TestBackbone(unittest.TestCase):
    def testEfficientNetLite(self):
        inputs = torch.rand(2, 3, 300, 300)
        model = efficient_lite_b2(cfg)
        features = model(inputs)
        for i, feat in enumerate(features):
            print(feat.size())
        # torch.Size([2, 40, 38, 38])
        # torch.Size([2, 112, 19, 19])
        # torch.Size([2, 320, 10, 10])
        # torch.Size([2, 256, 5, 5])
        # torch.Size([2, 256, 3, 3])
        # torch.Size([2, 256, 1, 1])

    def testEfficientNet(self):
        inputs = torch.rand(2, 3, 300, 300)
        model = efficient_net_b0(cfg)
        features = model(inputs)
        for i, feat in enumerate(features):
            print(feat.size())
        # torch.Size([2, 40, 38, 38])
        # torch.Size([2, 112, 19, 19])
        # torch.Size([2, 320, 10, 10])
        # torch.Size([2, 256, 5, 5])
        # torch.Size([2, 256, 3, 3])
        # torch.Size([2, 256, 1, 1])

    def testMobileNet(self):
        inputs = torch.rand(2, 3, 300, 300)
        model = MobileNetV2()
        features = model(inputs)
        # 0 torch.Size([2, 96, 19, 19])
        # 1 torch.Size([2, 1280, 10, 10])
        # 2 torch.Size([2, 512, 5, 5])
        # 3 torch.Size([2, 256, 3, 3])
        # 4 torch.Size([2, 256, 2, 2])
        # 5 torch.Size([2, 64, 1, 1])

        model = MobileNetV3()
        features = model(inputs)
        for i, feat in enumerate(features):
            print(i, feat.size())

        # 0 torch.Size([2, 96, 10, 10])
        # 1 torch.Size([2, 1280, 10, 10])
        # 2 torch.Size([2, 512, 5, 5])
        # 3 torch.Size([2, 256, 3, 3])
        # 4 torch.Size([2, 256, 2, 2])
        # 5 torch.Size([2, 64, 1, 1])
