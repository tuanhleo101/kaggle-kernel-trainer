import unittest

import numpy as np
import torch

from src.config.defaults import _C as cfg
from src.losses.focal_loss import FocalLoss
from src.losses.multibox_repulsion_loss import RepulsionMultiBoxLoss
from src.losses.my_multibox_loss import MultiBoxLoss
from src.modeling.anchors.prior_box import PriorBox


class TestLoss(unittest.TestCase):
    batch, n_anchors, num_classes = 2, 8732, 2
    pred_confs = torch.rand(batch, n_anchors, num_classes)
    pred_locs = torch.rand(batch, n_anchors, 4)
    target_labels = torch.tensor(np.random.randint(1, num_classes, (batch, n_anchors)))
    target_locs = torch.rand(batch, n_anchors, 4)
    priors = PriorBox(cfg)()

    def testMultiBoxLoss(self):
        neg_pos_ratio = 3
        loss_fn = MultiBoxLoss(num_classes=TestLoss.num_classes, neg_pos_ratio=neg_pos_ratio)
        print(loss_fn(TestLoss.pred_confs, TestLoss.pred_locs, TestLoss.target_labels, TestLoss.target_locs))

    def testFocalLoss(self):
        neg_pos_ratio = 3
        gamma = 2
        alpha = 0.25
        loss_fn = FocalLoss(num_classes=TestLoss.num_classes, neg_pos_ratio=neg_pos_ratio, gamma=gamma, alpha=alpha)
        print(loss_fn(TestLoss.pred_confs, TestLoss.pred_locs, TestLoss.target_labels, TestLoss.target_locs))

    def testMultiBoxRepulsionLoss(self):
        variance = (0.1, 0.2)
        neg_pos_ratio = 3
        loss_fn = RepulsionMultiBoxLoss(num_classes=TestLoss.num_classes, neg_pos_ratio=neg_pos_ratio,
                                        variance=variance)
        losses_dict = loss_fn(TestLoss.pred_confs, TestLoss.pred_locs, TestLoss.target_labels, TestLoss.target_locs,
                              **{"priors": TestLoss.priors})
        print(losses_dict)
