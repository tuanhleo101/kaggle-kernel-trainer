- [What is hard negative mining?](https://www.quora.com/What-is-hard-negative-mining)
    * For reducing the false part or negative samples
- [Loss]()
    * [x] Multibox loss
    * [x] Focal loss
    * [x] Repulsion loss
       + Stills bugs(fixed size of matched prediction box)
       + TODO: doesn't fix inheritance for focal repulsion loss 
- [BackBone]()
    * [x] EfficientNet-b0 with (pretrained), not have advprop
    * [x] EffcientNet_Lite-b(0->4) with not pretrained(train from raw)
    * [x] MobileNet(support v2, v3 currently)
    * [Everything you need to know about MobileNetV3](https://towardsdatascience.com/everything-you-need-to-know-about-mobilenetv3-and-its-comparison-with-previous-versions-a5d5e5a6eeaa)
- [Detector]()
    * [x] SSD
    * [ ] Efficient Det
        * [x] Box Head
        * [ ] Post Processor
- [Branch]()
    * Master
    * Develop
    * original
        * original-add-efficient-detector
- [Dataset]():
    * MOT:
        * [x] KITTI-17_ver_0)
        * [x] TUD-Stadtmitte_ver0
        * [x] PETS09-S2L1_ver0
        * [x] MOT20-01_ver0
        * [x] ignore MOT20-02 temporary
        * [ ] MOT17-04-SDP(ver0)
        * [x] MOT17-09-SDP(ver1)
        * [x] MOT17-11-SDP(remove)
        * [ ] MOT17-13-SDP
        * [ ] TUD-Campus
- [Training]():
    * focus efficient-lite + ssd(b0 -> b4)
    * mobilenet
    * efficient-lite + ssdlite*(b4->b0)
    * efficientDet
    * fssd